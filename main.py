import cv2
import time
import numpy as np


def main():
    video_source = 0
    cap = cv2.VideoCapture(video_source)
    if not cap.isOpened():
        print("No video")
        exit()

    fgbg = cv2.createBackgroundSubtractorMOG2()
    frame_rate_calc = 1
    freq = cv2.getTickFrequency()

    while True:
        t1 = cv2.getTickCount()
        
        ret, frame = cap.read()
        if not ret:
            print("No ret")
            break

        fgmask = fgbg.apply(frame)
        contours, _ = cv2.findContours(fgmask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        for contour in contours:
            if cv2.contourArea(contour) > 400:  # Минимальная площадь для считывания движения
                mask = np.zeros(frame.shape[:2], dtype="uint8")  # Создание черно-белой маски для текущего контура
                cv2.drawContours(mask, [contour], -1, 255, -1)  # Заполнение контура белым цветом на маске
                
                # Закрашиваем объект на исходном изображении, используя маску
                frame[mask == 255] = [255, 255, 255]  # Белый цвет в формате BGR
                cv2.putText(frame, "Motion detected!", (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)
        
        t2 = cv2.getTickCount()
        time1 = (t2-t1)/freq
        frame_rate_calc = 1/time1
        
        cv2.putText(frame,f'FPS: {frame_rate_calc:.2f}',(10,20),cv2.FONT_HERSHEY_SIMPLEX,0.7,(0,255,255),2)

        cv2.imshow('Default Video Window', frame)
        # cv2.imshow('Detect Move Window', fgmask)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    
    cap.release()

if __name__ == '__main__':
    try:
        main()
    finally:
        cv2.destroyAllWindows()
